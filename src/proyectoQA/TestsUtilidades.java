package proyectoQA;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestsUtilidades {

	@Test
	public void testDivision() {
		int resultado = Utilidades.division(10,2);
		int esperado = 5;
		assertEquals(resultado, esperado);
	}
	
	@Test
	public void testDivisionNegativo() {
		int resultado = Utilidades.division(10,0);
		int esperado = 5;
		assertEquals(resultado, esperado);
	}
	
	@Test 
	public void testExtraerNumero() {
		int resultado = Utilidades.extraerNumero("Proyecto1QA2017");
		int esperado = 1;
		assertEquals(resultado, esperado);
	}
	
	@Test 
	public void testExtraerNumeroIncorrecto() {
		int resultado = Utilidades.extraerNumero("Pro5yecto1QA2017");
		int esperado = 4;
		assertEquals(resultado, esperado);
	}
	
	

}
